package model.data;
import java.util.List;

import models.data.Stock;


import org.junit.Assert;
import org.junit.Test;

import common.AbstractTest;
public class StockTest extends AbstractTest{
	/**
	 * I ignored the testing of get/set method's because they are handled in the static testing.
	 * 	
	 * The professor said that we won't need to test these
	 * 
	 * **/
private int Default_num_of_stocks=5;// read yml file and see that we preallocate 5 stocks there.

	/**
	 * Checks for findAll
	 */
	@Test
	public void validfindAll() {
		List<Stock> temp=Stock.findAll();
		Assert.assertTrue(temp.size()==Default_num_of_stocks);}

}
