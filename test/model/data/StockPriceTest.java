package model.data;

import java.sql.Timestamp;
import java.util.List;

import models.data.Stock;
import models.data.StockPrice;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import common.AbstractTest;

/**
 * This class tests the models.data.StockPrice.
 * Google is the used as the basis of the data
 *
 * @author Susan Fung
 *
 */
public class StockPriceTest extends AbstractTest{
	private Stock stock;
	private StockPrice price;

	@Before
	public void getDummyData(){

		 stock = Stock.find.where().eq("ticker", "GOOG").findUnique();
		 DateTime dt = new DateTime(2013, 11, 20, 0, 0);
		 List<StockPrice> stockPrices = StockPrice.find.filter().eq("Date",new Timestamp(dt.getMillis())).filter(stock.getPrices());
		 price = stockPrices.get(0);
	}

	/**
	 * Tests if a stock price for a given date is between the low and high
	 * This also does boundary case of testing for low and high price
	 */
	@Test
    public void validQuotedPriceTest() {
		Assert.assertTrue(price.isValidQuotedPrice(1021));
		//low price
		Assert.assertTrue(price.isValidQuotedPrice(1020.36));
		//high price
		Assert.assertTrue(price.isValidQuotedPrice(1033.36));
    }


	/**
	 * This test case tests for boundaries that is slightly below the lowest price and slightly above the highest price
	 */
	@Test
    public void invalidQuotedPriceTest() {
		//.01 below the low price
		Assert.assertFalse(price.isValidQuotedPrice(1020.35));
		//.01 above the high price
		Assert.assertFalse(price.isValidQuotedPrice(1033.37));

    }

	/**
	 * Since the stock price is uses an auto generated number against a formula, this
	 * is to test that it always returns a valid stock price
	 */
	@Test
	public void validStockPrice()
	{
		for(int i=0; i<50;i++){
			Assert.assertTrue(price.isValidQuotedPrice(price.getStockPrice()));
		}
	}

}
