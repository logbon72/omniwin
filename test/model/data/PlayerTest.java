package model.data;
import java.util.List;

import models.data.Player;

import org.junit.Assert;
import org.junit.Test;

import common.AbstractTest;


/**
 * This test class tests the model.data.Player class
 * @author Susan Fung
 *
 */
public class PlayerTest extends AbstractTest{

	/**
	 * Tests that it returns all Players in Database
	 */
	@Test
	public void findAllPlayersInDB()
	{
		List<Player> players = Player.findAll();
		Assert.assertTrue(players.size()==1);
	}
}
