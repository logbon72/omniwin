package model.data;



import org.junit.Assert;

import common.AbstractTest;
public class AdminTest extends AbstractTest{
	private static int numOfAdmin=1;// prior knowledge from yml file
	
	
	/**
	 * test if Retrieves all users.
	 */
	public void validefindAll() {
		Assert.assertTrue(models.data.Admin.findAll().size()==numOfAdmin);
	}
	
}
