package model.data;
import java.sql.Timestamp;

import managers.UserManager;
import models.data.Game;
import models.data.GamePlayer;
import models.data.Player;
import models.data.User;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import com.avaje.ebean.Ebean;

import common.AbstractTest;

public class GameTest extends AbstractTest{
	/**
	 * I ignored the testing of get/set method's because they are handled in the static testing.
	 * 	
	 * The professor said that we won't need to test these
	 * 
	 * **/
	@Mock
	private Game game;
	/**
     * test to Retrieve a Gmae using id.
     */
	public void validefindById(){
		int id=0;
		Game temp=Game.findById(id);
		Assert.assertTrue(temp.getId()==id);
	}
	/**
     * test if a player can still join a game
     *
     */
    public void validecanJoinGame() {
    	Assert.assertTrue((new Game()).canJoinGame(new Player()));
    }
    /**
     * Test if a player join game normally 
     */
    public void validjoinGame() {
    	Assert.assertTrue((new Game()).joinGame(new Player())!=null);
    }
}
