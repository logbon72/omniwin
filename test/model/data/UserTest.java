package model.data;


import models.data.User;

import org.junit.Assert;
import org.junit.Test;

import common.AbstractTest;

/**
 * This class tests the models.data.User
 * @author susan.fung
 *
 */
public class UserTest extends AbstractTest{


	/**
	 * Checks for valid email
	 */
	@Test
	public void validFindByEmail() {
		User user = User.findByEmail("sgf2110@columbia.edu");
		Assert.assertTrue(user!=null);
	}


	/**
	 * Checks for invalid email
	 */
	@Test
	public void invalidFindByEmail() {
		User user = User.findByEmail("abc");
		Assert.assertTrue(user==null);
	}


	/**
	 * Checks for valid authentication for logging in
	 */
	@Test
	public void authenticateSuccess() {
		User user = User.authenticate("sgf2110@columbia.edu", "abc");
		Assert.assertTrue(user!=null);
	}

	/**
	 * Checks for authentication failure
	 */
	@Test
	public void authenticateFailed() {
		User user = User.authenticate("sgf2110@columbia.edu", "abcd");
		Assert.assertTrue(user==null);
	}
}
