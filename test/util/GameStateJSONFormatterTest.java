/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import beans.GameState;
import com.fasterxml.jackson.databind.node.ObjectNode;
import common.AbstractTest;
import java.sql.Timestamp;
import java.util.ArrayList;
import managers.GameManager;
import models.data.FixedTimeChallengeGame;
import models.data.GamePlayer;
import models.data.Player;
import models.data.PortfolioStock;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author intelWorX
 */
public class GameStateJSONFormatterTest extends AbstractTest {

    public static final int CURRENT_GAME_DAY = 2;
    public static final Timestamp VIRTUAL_CURRENT_DAY = new Timestamp(1336089600000L);
    public static final double OPENING_BALANCE = 500.0;
    public static final int MAX_PLAYERS = 5;
    public static final double GAME_BALANCE = 550.0;
    public static final int GAME_ID = 1;
    public static final int PLAYER_ID = 20;
    public static final String LAST_NAME = "lastName";
    public static final String FIRST_NAME = "firstName";
    @Mock
    private GameState gameState;
    @Mock
    private FixedTimeChallengeGame game;

    @Mock
    private GamePlayer gameOwner;

    @Mock
    private Player user;

    @InjectMocks
    private GameStateJSONFormatter gameStateJSONFormatter;
    public static final String TEST_GAME = "TestGame";
    public static final int GAME_DURATION = 7;

    public static final int testGameLength = GAME_DURATION;

    public GameStateJSONFormatterTest() {

    }

    @Before
    public void createStubs() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.when(user.getFirstName()).thenReturn(FIRST_NAME);
        Mockito.when(user.getLastName()).thenReturn(LAST_NAME);
        //set up owner
        Mockito.when(gameOwner.getId()).thenReturn(PLAYER_ID);
        Mockito.when(gameOwner.getUser()).thenReturn(user);
        Mockito.when(gameOwner.getCurrentBalance()).thenReturn(GAME_BALANCE);
        Mockito.when(gameOwner.getNetWorth()).thenReturn(GAME_BALANCE);
        Mockito.when(gameOwner.getPortfolios()).thenReturn(new ArrayList<PortfolioStock>());

        //set up game
        Mockito.when(game.getId()).thenReturn(GAME_ID);
        Mockito.when(game.getGameTitle()).thenReturn(TEST_GAME);
        Mockito.when(game.getGameLength()).thenReturn(GAME_DURATION);
        Mockito.when(game.getGameStatus()).thenReturn(enums.GameStatusEnum.STARTED.getValue());
        Mockito.when(game.getMaxPlayers()).thenReturn(MAX_PLAYERS);
        Mockito.when(game.getOpenBalance()).thenReturn(OPENING_BALANCE);
        //not really needed for test.
        Mockito.when(game.getVirtualCurrentDate()).thenReturn(VIRTUAL_CURRENT_DAY);

        //set up game state
        Mockito.when(gameState.getGame()).thenReturn(game);

        Mockito.when(gameState.getCurrentDay()).thenReturn(CURRENT_GAME_DAY);
        Mockito.when(gameState.getNumOfDays()).thenReturn(GAME_DURATION);
        Mockito.when(gameState.getCurrentPlayer()).thenReturn(gameOwner);
        Mockito.when(gameState.getGameOwner()).thenReturn(gameOwner);
        ArrayList<GamePlayer> players = new ArrayList<GamePlayer>();
        players.add(gameOwner);
        Mockito.when(gameState.getPlayers()).thenReturn(players);

        gameStateJSONFormatter = new GameStateJSONFormatter(gameState);
    }

    /**
     * Test of getJSON method, of class GameStateJSONFormatter.
     */
    @Test
    public void testGetJSON() {   
        System.out.println("getJSON");
        //Mockito.when(expResult).thenReturn(expResult);
        ObjectNode gameStateJson = gameStateJSONFormatter.getJSON();
        assertObjectNode(gameStateJson);
    }

    protected void assertObjectNode(ObjectNode gameStateJson) {
        assertNotNull(gameStateJson);
        assertEquals(gameStateJson.get("id").asInt(), GAME_ID);
        assertEquals(gameStateJson.get("title").asText(), TEST_GAME);
        assertEquals(gameStateJson.get("owner").get("firstName").asText(), FIRST_NAME);
        assertEquals(gameStateJson.get("owner").get("lastName").asText(), LAST_NAME);
        assertEquals(gameStateJson.get("owner").get("id").asInt(), PLAYER_ID);
        assertEquals(gameStateJson.get("currentGameDay").asInt(), CURRENT_GAME_DAY);
        assertEquals(gameStateJson.get("players").size(), 1);
        assertEquals(gameStateJson.get("gameStatus").asText(), enums.GameStatusEnum.STARTED.getValue());
        assertEquals(gameStateJson.get("numberOfDays").asInt(), GAME_DURATION);
        assertEquals(gameStateJson.get("currentPlayer").get("id").asInt(), gameStateJson.get("owner").get("id").asInt());
        assertEquals(GAME_BALANCE, gameStateJson.get("netWorth").asDouble(), 0);
        assertEquals(gameStateJson.get("maxPlayers").asInt(), MAX_PLAYERS);
        assertEquals(gameStateJson.get("myAccount").get("openingBalance").asDouble(), OPENING_BALANCE, 0);
        assertEquals(gameStateJson.get("myAccount").get("currentBalance").asDouble(), GAME_BALANCE, 0);
    }
    

}
