package util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import models.data.Game;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import common.AbstractTest;
import exceptions.InvalidDateException;

@RunWith(MockitoJUnitRunner.class)
public class TimeKeeperTest extends AbstractTest{
	@Mock
	private Game game;

	/**
	 * This test for round_to_day(Timestamp raw)
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void validround_to_day(){
		Calendar now = Calendar.getInstance(); 
		Timestamp temp=new Timestamp(now.getTimeInMillis());
			Timestamp today=TimeKeeper.round_to_day(temp);
			Assert.assertTrue((now.getTime().getDay()-today.getDay())==0);
	}
	/**
	 * This test for convert(Timestamp raw) that gives date that is before the real start of the game
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void validconvert() {
		Timestamp temp=TimeKeeper.convert(game);
		Assert.assertTrue(temp.before(new Date()));
	}
	/**
	 * This test for getLastTradingDay(Game game)
	 */
	@Test
	 public void validgetLastTradingDay() {
		 Timestamp temp=TimeKeeper.getLastTradingDay( game);
		 Assert.assertTrue(temp.before(new Date())&&temp.after(game.getRealStartTime()));
	    }
	
	
}
