package util;

import java.sql.Timestamp;

import models.data.Game;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import common.AbstractTest;

import exceptions.InvalidDateException;

@RunWith(MockitoJUnitRunner.class)
public class GameUtilTest extends AbstractTest {

	@Mock
	private Game game;


	/**
	 * This test for a valid test case to get the current number of day of the game
	 */
	@Test
	public void validGetCurrentDay()
	{
		//stub values
		DateTime dt = new DateTime();
		Mockito.when(game.getVirtualCurrentDate()).thenReturn(GameUtil.getCurrentTimeStamp());
		DateTime modifiedDt = dt.minusDays(3);
		Timestamp startDt = new Timestamp(modifiedDt.getMillis());
		Mockito.when(game.getVirtualStartDate()).thenReturn(startDt);

		try{
			int currentDay = GameUtil.getCurrentDay(game);
			Assert.assertTrue(currentDay>-1);
			Assert.assertEquals(currentDay, 3);
		}
		catch (InvalidDateException ide)
		{
			ide.printStackTrace();
		}
		
	}

	/**
	 * This is a boundary test case
	 * This test for a valid test case to that it day 0 is returned if the current date matches start date
	 */
	@Test
	public void validGetCurrentDayEq0()
	{
		//stub values
		Mockito.when(game.getVirtualCurrentDate()).thenReturn(GameUtil.getCurrentTimeStamp());
		Mockito.when(game.getVirtualStartDate()).thenReturn(GameUtil.getCurrentTimeStamp());
		try{
			int currentDay = GameUtil.getCurrentDay(game);

			Assert.assertTrue(currentDay>-1);
			Assert.assertEquals(currentDay, 0);
		}
		catch (InvalidDateException ide)
		{
			ide.printStackTrace();
		}
	}

	/**
	 * This should throw an exception if the virtual start date is a date in the future of the current date
	 */
	@Test(expected = exceptions.InvalidDateException.class)
	public void invalidGetCurrentDay()
		throws InvalidDateException
	{
		//stub values
		Mockito.when(game.getVirtualCurrentDate()).thenReturn(GameUtil.getCurrentTimeStamp());
		DateTime modifiedDt = new DateTime().plusDays(3);
		Timestamp startDt = new Timestamp(modifiedDt.getMillis());
		Mockito.when(game.getVirtualStartDate()).thenReturn(startDt);

//		try{
			GameUtil.getCurrentDay(game);
//		}
//		catch (InvalidDateException ide)
//		{
//			ide.printStackTrace();
//		}
	}
}
