package common;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.start;

import org.junit.Before;

/**
 * Test cases that wish to spin up a fake server with in memory database should extend this class
 * 
 * @author Susan Fung
 *
 */
public abstract class AbstractTest {
	
	/** 
	 * Performed before every test case is ran.  It creates a dummy server using an in memory database
	 */
	@Before
	public void setUp() {

	    start(fakeApplication(inMemoryDatabase()));
	}
	

}
