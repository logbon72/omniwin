package managers;

import java.io.IOException;
import java.sql.Timestamp;

import model.forms.GameForm;
import models.data.Game;
import models.data.StockPrice;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import play.mvc.Http.Context;

import common.AbstractTest;

import constants.ApplicationConstants;
import exceptions.InvalidStockException;

@RunWith(MockitoJUnitRunner.class)
public class GameManagerTest extends AbstractTest {
	@Mock
	private GameForm gameForm;
	@Mock
	private Game game;

	@InjectMocks
    private GameManager gameManager;

	private Timestamp date;


	@Before
	public void createStubs(){
		Context context = Mockito.mock(Context.class);
		Context.current.set(context);
		Mockito.when(gameForm.gameLength).thenReturn(2);
		Mockito.when(gameForm.gameTitle).thenReturn("Test game");
		Mockito.when(gameForm.maxPlayers).thenReturn(2);
		Mockito.when(gameForm.openBalance).thenReturn(1000.00);
		DateTime dt = new DateTime(2013,11,22,0,0);
		date = new Timestamp(dt.getMillis());
	}

	/**
	 * This is a good scenario of adding a stock
	 *
	 * @throws IOException
	 * @throws InvalidStockException
	 */
	@Test
	public void addStockSuccessTest()
		throws IOException, InvalidStockException{
       Assert.assertTrue(gameManager.addStock("TRI", "Reuters", ApplicationConstants.TRUE));
    }

	/**
	 * This checks that the company is not empty
	 *
	 * @throws IOException
	 * @throws InvalidStockException
	 */
	@Test(expected = InvalidStockException.class)
	public void addStockWithMissingCompanyNameTest()
		throws IOException, InvalidStockException{
       Assert.assertFalse(gameManager.addStock("UBS", "", ApplicationConstants.TRUE));
       Assert.assertFalse(gameManager.addStock("UBS", null, ApplicationConstants.TRUE));
    }

	/**
	 * This checks that the enabled indicator is valid
	 *
	 * @throws IOException
	 * @throws InvalidStockException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void addStockWithInvalidEnabledStatusNameTest()
		throws IOException, InvalidStockException{
       Assert.assertFalse(gameManager.addStock("UBS", "UBS AG", 3));
    }

	/**
	 * This test case covers 2 methods, addStock and stockExistInSystem
	 * @throws IOException
	 */
	@Test
	public void addStockThatisAlreadyInSystemTest()
		throws IOException, InvalidStockException{
       Assert.assertFalse(gameManager.addStock("GOOG", "Google", ApplicationConstants.TRUE));
    }

	/**
	 * This test case covers 2 methods, addStock and stockExist
	 * @throws IOException
	 */
	@Test
	public void addStockWithInvalidTickerTest()
		throws IOException, InvalidStockException{
       Assert.assertFalse(gameManager.addStock("#$%$", "Non existent ticker", ApplicationConstants.TRUE));
    }

	/**
	 * This tests for empty tickers
	 *
	 * @throws IOException
	 * @throws InvalidStockException
	 */
	@Test(expected = InvalidStockException.class)
	public void addStockWithEmptyTickerTest()
		throws IOException, InvalidStockException{
       Assert.assertFalse(gameManager.addStock("", "Non existent ticker", ApplicationConstants.TRUE));
       Assert.assertFalse(gameManager.addStock(null, "Non existent ticker", ApplicationConstants.TRUE));
    }


	/**
	 * Good scenario to delete stock
	 *
	 * @throws IOException
	 */
	@Test
	public void deleteStockSuccessTest()
		throws IOException, InvalidStockException{
		Assert.assertTrue(gameManager.deleteStock("GOOG"));
	}

	/**
	 * This tests to attempt to delete a stock not in system
	 * @throws IOException
	 */
	@Test
	public void deleteStockFailureTest()
		throws IOException, InvalidStockException{
		Assert.assertFalse(gameManager.deleteStock("UBS"));
	}

	/**
	 * This tests to attempt to delete a stock not in system
	 * @throws IOException
	 */
	@Test(expected = InvalidStockException.class)
	public void deleteStockwithBlankTickerTest()
		throws IOException, InvalidStockException{
		Assert.assertFalse(gameManager.deleteStock(""));
		Assert.assertFalse(gameManager.deleteStock(null));
	}

	@Test(expected = InvalidStockException.class)
	public void flipEnableStockWithBlankTickerTest()
		throws IOException, InvalidStockException{
		gameManager.flipEnableStock("");
		gameManager.flipEnableStock(null);
	}

	@Test
	public void flipEnableStockSuccessTest()
		throws IOException, InvalidStockException{
		Assert.assertEquals(gameManager.flipEnableStock("GOOG"),ApplicationConstants.FALSE);
	}

	@Test(expected = InvalidStockException.class)
	public void flipEnableStockNotInSystemTest()
		throws IOException, InvalidStockException{
		gameManager.flipEnableStock("UBS");
	}


	@Test(expected = InvalidStockException.class)
	public void getStockPriceFailureTest()
		throws IOException, InvalidStockException{
		gameManager.getStockPrice("", null);
		gameManager.getStockPrice(null, null);
		gameManager.getStockPrice("GOOG",null);
	}

	@Test
	public void getStockPriceNotInSystemTest()
		throws IOException, InvalidStockException{
		Assert.assertTrue(gameManager.getStockPrice("UBS",date)==null);
	}

	@Test
	public void getStockPriceSuccessTest()
		throws IOException, InvalidStockException{
		StockPrice stockPrice = gameManager.getStockPrice("GOOG",date);
		Assert.assertTrue(stockPrice!=null);
	}

	@Test(expected = InvalidStockException.class)
	public void getStockPriceAmountFailureTest()
		throws IOException, InvalidStockException{
		gameManager.getStockPriceAmount("", null);
		gameManager.getStockPriceAmount(null, null);
		gameManager.getStockPriceAmount("GOOG",null);
	}

	@Test
	/**
	 * Need to decide with team what should output be???
	 * @throws IOException
	 * @throws InvalidStockException
	 */
	public void getStockPriceAmountNotInSystemTest()
		throws IOException, InvalidStockException{
		gameManager.getStockPriceAmount("UBS",date);
	}

	@Test
	public void getStockPriceAmountSuccessTest()
		throws IOException, InvalidStockException{
		double amount = gameManager.getStockPriceAmount("GOOG",date);
		Assert.assertTrue(amount>0.00);
	}
	

}
