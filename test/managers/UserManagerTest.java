package managers;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import model.forms.GameForm;
import models.data.Game;
import models.data.StockPrice;
import models.data.User;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import play.mvc.Controller;
import play.mvc.Http.Context;
import common.AbstractTest;
import constants.ApplicationConstants;
import exceptions.InvalidStockException;
@RunWith(MockitoJUnitRunner.class)
public class UserManagerTest extends AbstractTest{
	@Before
	public void createStubs(){
		Context context = Mockito.mock(Context.class);
		Context.current.set(context);
		/*
		Mockito.when(gameForm.gameLength).thenReturn(2);
		Mockito.when(gameForm.gameTitle).thenReturn("Test game");
		Mockito.when(gameForm.maxPlayers).thenReturn(2);
		Mockito.when(gameForm.openBalance).thenReturn(1000.00);
		DateTime dt = new DateTime(2013,11,22,0,0);
		date = new Timestamp(dt.getMillis());*/
	}
	@Mock
	private Game game;

	@InjectMocks
    private UserManager userManager= new UserManager();

	/**
	 * This is a good scenario of getting a logged in user
	 *
	 */
	@Test
	public void getCurrentLoggedInUserTest() {
        Assert.assertTrue(UserManager.getCurrentLoggedInUser()!=null);
    }
	/**
	 * This is testing if isLoggedIn is returning well
	 *
	 */
	@Test
	public void isLoggedInTest() {
        Assert.assertTrue(UserManager.isLoggedIn()==true);
    }
	/**
	 * This is testing getUserList is returning right size of list
	 *
	 */
	@Test
	public void getUserListTest() {
		List<User> temp=UserManager.getUserList();
		System.out.println("Let's count the numb of User: "+temp.size());
        Assert.assertTrue(UserManager.getUserList().size()==2);
    }
	

}
