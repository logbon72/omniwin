

import model.forms.RegisterForm;

import org.junit.Assert;
import org.junit.Test;

import common.AbstractTest;

import constants.ApplicationConstants;

public class RegistrationTest extends AbstractTest{

	@Test
    public void passwordMismatch() {
		RegisterForm registerForm = new RegisterForm();
		registerForm.email="susan.g.fung@gmail.com";
		registerForm.firstName="Susan";
		registerForm.lastName="Fung";
		registerForm.password="abc";
		registerForm.password2="abcd";
		String message = registerForm.validate();
		Assert.assertTrue(message.equals(ApplicationConstants.PASSWORD_MISMATCH));
    }

	@Test
    public void userAlreadyRegistered() {
		RegisterForm registerForm = new RegisterForm();
		registerForm.email="sgf2110@columbia.edu";
		registerForm.firstName="Susan";
		registerForm.lastName="Fung";
		registerForm.password="abc";
		registerForm.password2="abc";
		registerForm.isEnabled=ApplicationConstants.TRUE;
		String message = registerForm.validate();
		Assert.assertTrue(message.equals(ApplicationConstants.USER_ALREADY_REGISTERED));
    }

	@Test
    public void successRegistration() {
		RegisterForm registerForm = new RegisterForm();
		registerForm.email="susan.fung@gmail.com";
		registerForm.firstName="Susan";
		registerForm.lastName="Fung";
		registerForm.password="abc";
		registerForm.password2="abc";
		registerForm.isEnabled=ApplicationConstants.TRUE;
		String message = registerForm.validate();
		Assert.assertTrue(message == null);
    }

}
