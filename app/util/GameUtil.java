package util;

import java.sql.Timestamp;

import models.data.Game;

import org.joda.time.DateTime;
import org.joda.time.Days;

import exceptions.InvalidDateException;

/**
 *
 * @author Susan Fung
 *
 *         This is a utility class for game related activity
 */
public class GameUtil {


	/**
	 * This returns the current day. Meaning the number of days from the start
	 * date
	 *
	 * @param game
	 * @return
	 */
	public static int getCurrentDay(Game game)
		throws InvalidDateException{
		DateTime startDate = new DateTime(game.getVirtualStartDate().getTime());
		DateTime currentDate = new DateTime(game.getVirtualCurrentDate().getTime());
		if (startDate.isAfter(currentDate))
			throw new InvalidDateException("Virtual start date cannot be after virtual current date.");
		// got from
		// http://stackoverflow.com/questions/3802893/number-of-days-between-two-dates-in-joda-time
		int diff = Days.daysBetween(startDate.toDateMidnight(),
				currentDate.toDateMidnight()).getDays();

		return diff - game.getVirtualDaysSkipped();
	}

	/**
	 * Gets current system date
	 *
	 * @return
	 */
	public static DateTime getCurrentDate() {
		return new DateTime();
	}

	/**
	 * Gets current system timestamp
	 *
	 * @return
	 */
	public static Timestamp getCurrentTimeStamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * This method checks if a string is blank or null
	 *
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str)
	{
		if (str==null || str.trim().equals(""))
			return true;

		return false;
	}

}
