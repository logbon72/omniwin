package exceptions;

public class InvalidStockException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidStockException(String message) {
		super(message);
	}

	public InvalidStockException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public InvalidStockException(Throwable cause) {
        super(cause);
    }

}
