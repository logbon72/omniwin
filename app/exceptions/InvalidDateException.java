package exceptions;

public class InvalidDateException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidDateException(String message) {
		super(message);
	}

	public InvalidDateException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public InvalidDateException(Throwable cause) {
        super(cause);
    }

}
