package enums;

public enum EnabledStatusEnum {

    TRUE(1),
    FALSE(0);

    private final int status;

    EnabledStatusEnum(int status) {
        this.status = status;
    }

    public final int getStatus() {
        return status;
    }

    public static EnabledStatusEnum forValue(final int status) {
        for (EnabledStatusEnum e : values()) {
            if (e.status == status) {
                return e;
            }
        }
        throw new IllegalArgumentException("No Enum Found. Invalid Enabled Status: " + status);
    }

    public final int getValue() {
        return status;
    }

    public final String toString() {
        return String.valueOf(status);
    }

}
